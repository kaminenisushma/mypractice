const fibonacci = (n: number): number[] => {
  const arr: number[] = [];
  let a = 1;
  let b = 1;
  arr.push(a);
  arr.push(b);
  for (let i = 2; i < n; i += 1) {
    arr[i] = a + b;
    a = b;
    b = arr[i];
  }
  return arr;
};
export default fibonacci;

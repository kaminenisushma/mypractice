import gcd from './gcd';
export default class Rational {
  readonly numerator: number;
  readonly denominator: number;
  constructor(numerator = 0, denominator = 1) {
    const g = gcd(numerator, denominator);
    this.numerator = numerator / g;
    this.denominator = denominator / g;
  }
  add = (r: Rational): Rational => {
    const n = this.numerator * r.denominator + this.denominator * r.numerator;
    const d = this.numerator * r.denominator;
    return new Rational(n, d);
  };
  subtract = (r: Rational): Rational => {
    const n = this.numerator * r.denominator - this.denominator * r.numerator;
    const d = this.denominator * r.denominator;
    return new Rational(n, d);
  };
  multiply = (r: Rational): Rational => {
    const n = this.numerator * r.numerator / this.denominator * r.denominator;
    return new Rational(n);
  };
  division = (r: Rational): Rational => {
    const n = this.numerator / this.denominator * r.numerator / r.denominator;
    return new Rational(n);
  };
}

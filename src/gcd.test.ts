import gcd from './gcd';
test('gcd', () => {
  expect(gcd(2, 4)).toEqual(2);
  expect(gcd(1, 5)).toEqual(1);
  expect(gcd(2, 4)).toEqual(2);
});

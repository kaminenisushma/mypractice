import { concat2 } from './mapcat';
const flatten = (arr: number[][]): number[] => {
  let result: number[] = [];
  for (const e of arr) {
    result = concat2(result, e);
  }
  return result;
};
export default flatten;

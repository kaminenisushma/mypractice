const factorial = (n: number) => {
  let fact = 1;
  for (let i = 0; i <= n; i += 1) {
    fact *= i;
  }
  return fact;
};
export default factorial;

// import {concat2} from './mapcat';
import flatten from './flatten';
test('flatten', () => {
  expect(flatten([[1, 2, 3, 4], [5, 6, 7], [9, 10, 11]])).toEqual([
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    9,
    10,
    11
  ]);
  expect(flatten([[1, 2, 3, 4], [5, 6, 7], [8, 9]])).toEqual([
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9
  ]);
  expect(flatten([[1, 2, 3, 4], [5]])).toEqual([1, 2, 3, 4, 5]);
  expect(flatten([[]])).toEqual([]);
});

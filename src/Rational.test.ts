import Rational from './Rational';

test('constructor', () => {
  let r = new Rational();
  expect(r.numerator).toEqual(0);
  expect(r.denominator).toEqual(1);

  r = new Rational(10);
  expect(r.numerator).toEqual(10);
  expect(r.denominator).toEqual(1);

  r = new Rational(1, 2);
  expect(r.numerator).toEqual(1);
  expect(r.denominator).toEqual(2);

  r = new Rational(2, 4);
  expect(r.numerator).toEqual(1);
  expect(r.denominator).toEqual(2);
});

test('equals', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(3, 6);
  const r3 = new Rational(1, 3);
  expect(r.equals(r2)).toBeTruthy();
  expect(r.equals(r3)).toBeFalsy();
});

test('add', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const expected = new Rational(7, 6);
  const r3 = r.add(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('sub', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const expected = new Rational(7, 6);
  const r3 = r.subtract(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('mul', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const expected = new Rational(7, 6);
  const r3 = r.multiply(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('div', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const expected = new Rational(7, 6);
  const r3 = r.division(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('compare', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);

  const expected = new Rational(7, 6);
  const r3 = r.compare(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
